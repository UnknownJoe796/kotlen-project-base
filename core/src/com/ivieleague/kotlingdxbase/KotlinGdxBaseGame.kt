package com.ivieleague.kotlingdxbase

import com.badlogic.gdx.Game
import com.ivieleague.kotlen.behavior.BehaviorToScreen
import com.ivieleague.kotlen.test.dots.DotTest

class KotlinGdxBaseGame : Game() {
    override fun create() {
        setScreen(BehaviorToScreen(DotTest()))
    }
}
